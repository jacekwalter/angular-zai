angular.module("productsListing", [])
    .controller('Products', function($scope) {

        $scope.orderby = ["name", "price", "color", "size"];
        $scope.order = "";

        $scope.priceLow = null;
        $scope.priceHigh = null;

        $scope.products = [{
                "name": "KKB t-shirt",
                "price": 32,
                "description": "Beautiful pink t-shirt",
                "color": "pink",
                "available": "Yes",
                "picture_url": "https://images.bigcartel.com/product_images/176331542/2.jpg",
                "size": "medium"
            },
            {
                "name": "FSO Polonez",
                "price": 900,
                "description": "Motor vehicle",
                "color": "green",
                "available": "1 month",
                "picture_url": "http://gminazlota.tbu.pl/wiadomosci/foto/polonez2.JPG",
                "size": "very big"
            },
            {
                "name": "Cisowianka zgrzewka",
                "price": 4,
                "description": "Lots of water",
                "color": "transparent",
                "available": "Yes",
                "picture_url": "http://www.bdsklep.pl/gfx/cisowianka-6-1-5l-woda-lekko-gazowana-zgrzewka.jpg",
                "size": "medium"
            },
            {
                "name": "Palm tree",
                "price": 400,
                "description": "A real palm tree",
                "color": "green",
                "available": "No",
                "picture_url": "http://www.palmtreepassion.com/images/christmas_palm.jpg",
                "size": "big"
            },
            {
                "name": "Ruby gem",
                "price": 2000,
                "description": "Beautiful ruby gem",
                "color": "red",
                "available": "2 months",
                "picture_url": "http://bkgstory.com/wp-content/uploads/2014/10/4-01cts_burmaruby_optiononev7.jpg",
                "size": "small"
            }
        ];

        var empty = {
            "name": "",
            "price": "",
            "description": "",
            "color": "",
            "available": "",
            "picture_url": "",
            "size": ""
        };

        $scope.temp_product = angular.copy(empty);

        $scope.addProduct = function(product) {
            $scope.products.push(product);
            $scope.temp_product = angular.copy(empty);
        }

        $scope.removeAt = function(index) {
            $scope.products.splice($scope.products.indexOf(index), 1);
        }

        $scope.priceFilter = function(entry) {
            var low = -Infinity;
            var high = Infinity;
            if ($scope.priceLow !== null) {
                low = $scope.priceLow;
            }
            if ($scope.priceHigh !== null) {
                high = $scope.priceHigh;
            }
            return entry.price >= low && entry.price <= high;
        }

    })

.directive('product', function() {
    return {
        scope: {
            entry: '=?bind',
            index: '@',
            removeAt: '&'
        },
        templateUrl: 'product.html',
    };
});
